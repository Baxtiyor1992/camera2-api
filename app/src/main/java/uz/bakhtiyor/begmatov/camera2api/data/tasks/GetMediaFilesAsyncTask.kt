package uz.bakhtiyor.begmatov.camera2api.data.tasks

import android.os.AsyncTask
import uz.bakhtiyor.begmatov.camera2api.data.models.MediaFile
import uz.bakhtiyor.begmatov.camera2api.data.repos.MediaFilesRepo

class GetMediaFilesAsyncTask(
    private val mediaFilesRepo: MediaFilesRepo,
    private val callback: (List<MediaFile>) -> Unit
) : AsyncTask<Void, Void, List<MediaFile>>() {

    override fun doInBackground(vararg params: Void?): List<MediaFile> {
        return mediaFilesRepo.getImagesAndVideos()
    }

    override fun onPostExecute(result: List<MediaFile>?) {
        callback.invoke(result ?: listOf())
    }
}