package uz.bakhtiyor.begmatov.camera2api

import android.app.Application
import android.content.Context
import uz.bakhtiyor.begmatov.camera2api.data.repos.MediaFilesRepo
import uz.bakhtiyor.begmatov.camera2api.data.repos.impl.MediaFilesRepoImpl

class App : Application() {

    lateinit var settings: AppSettings; private set
    lateinit var mediaFilesRepo: MediaFilesRepo; private set

    override fun onCreate() {
        super.onCreate()

        settings = AppSettings.getInstance(this)
        mediaFilesRepo = MediaFilesRepoImpl(this)
    }

    companion object {

        @JvmStatic
        fun get(context: Context): App {
            return context.applicationContext as App
        }
    }
}