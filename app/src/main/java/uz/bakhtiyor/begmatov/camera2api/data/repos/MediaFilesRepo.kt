package uz.bakhtiyor.begmatov.camera2api.data.repos

import android.media.Image
import uz.bakhtiyor.begmatov.camera2api.data.models.MediaFile

interface MediaFilesRepo {

    fun getImagesAndVideos(): List<MediaFile>

    fun saveMediaFile(image: Image, fileName: String)
}