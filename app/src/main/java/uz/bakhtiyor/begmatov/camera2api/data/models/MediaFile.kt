package uz.bakhtiyor.begmatov.camera2api.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MediaFile(
    val id: Int,
    val name: String,
    val mimeType: Int,
    val absolutePath: String?
) : Parcelable