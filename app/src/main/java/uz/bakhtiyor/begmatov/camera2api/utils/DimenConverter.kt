package uz.bakhtiyor.begmatov.camera2api.utils

import android.content.Context
import android.util.TypedValue

class DimenConverter(context: Context) {

    private val displayMetrics = context.resources!!.displayMetrics

    fun dp2px(dp: Float): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics)
    }
}