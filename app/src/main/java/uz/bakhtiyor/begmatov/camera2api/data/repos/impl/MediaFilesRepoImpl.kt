package uz.bakhtiyor.begmatov.camera2api.data.repos.impl

import android.content.ContentValues
import android.content.Context
import android.media.Image
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.annotation.RequiresApi
import uz.bakhtiyor.begmatov.camera2api.data.models.MediaFile
import uz.bakhtiyor.begmatov.camera2api.data.repos.MediaFilesRepo
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

class MediaFilesRepoImpl(context: Context) : MediaFilesRepo {

    private val resolver = context.contentResolver

    override fun getImagesAndVideos(): List<MediaFile> {
        val projection = arrayOf(
            MediaStore.Files.FileColumns._ID,
            MediaStore.Files.FileColumns.DISPLAY_NAME,
            MediaStore.Files.FileColumns.MEDIA_TYPE,
            MediaStore.Files.FileColumns.DATA
        )
        val mediaTypeColumn = MediaStore.Files.FileColumns.MEDIA_TYPE
        val imageMimeType = MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
        val videoMimeType = MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO

        val selection = "$mediaTypeColumn = $imageMimeType OR $mediaTypeColumn = $videoMimeType"
        val cursor = resolver.query(
            MediaStore.Files.getContentUri("external"),
            projection,
            selection,
            null,
            MediaStore.Files.FileColumns.DATE_ADDED + " DESC"
        )

        val result = mutableListOf<MediaFile>()

        cursor?.use {
            cursor.moveToFirst()

            while (!cursor.isAfterLast) {
                result.add(
                    MediaFile(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getInt(2),
                        cursor.getString(3)
                    )
                )

                cursor.moveToNext()
            }
        }

        return result
    }

    override fun saveMediaFile(image: Image, fileName: String) {
        val outputStream = (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            openImageFileOutputStream(fileName)
        } else {
            openImageFileOutputStreamLegacy(fileName)
        }) ?: throw RuntimeException("Unable save image file")

        val buffer = image.planes[0].buffer
        val bytes = ByteArray(buffer.remaining())

        buffer[bytes]

        outputStream.use {
            outputStream.write(bytes)
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun openImageFileOutputStream(fileName: String): OutputStream? {
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            put(
                MediaStore.MediaColumns.RELATIVE_PATH,
                Environment.DIRECTORY_DCIM + File.separator
            )
        }

        return resolver.openOutputStream(
            resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)!!
        )
    }

    @Suppress("DEPRECATION")
    private fun openImageFileOutputStreamLegacy(fileName: String): OutputStream? {
        val imageFile = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
            fileName
        )
        return FileOutputStream(imageFile)
    }
}