package uz.bakhtiyor.begmatov.camera2api.utils

import android.content.Context
import android.util.AttributeSet
import android.view.TextureView

class AutoFitTextureView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : TextureView(context, attrs, defStyle) {

    private var ratioWidth = 1
    private var ratioHeight = 1

    fun setRatio(ratioWidth: Int, ratioHeight: Int) {
        if (ratioWidth < 0 || ratioHeight < 0) {
            throw IllegalArgumentException("Ratio must be greater than zero")
        }
        this.ratioWidth = ratioWidth
        this.ratioHeight = ratioHeight
        requestLayout()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)

        if (width < height * ratioWidth / ratioHeight) {
            setMeasuredDimension(width, width * ratioHeight / ratioWidth)
        } else {
            setMeasuredDimension(height * ratioWidth / ratioHeight, height)
        }
    }
}