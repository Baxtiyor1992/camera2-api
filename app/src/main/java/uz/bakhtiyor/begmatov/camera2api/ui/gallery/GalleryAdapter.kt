package uz.bakhtiyor.begmatov.camera2api.ui.gallery

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import uz.bakhtiyor.begmatov.camera2api.R
import uz.bakhtiyor.begmatov.camera2api.data.models.MediaFile
import java.io.File

class GalleryAdapter : ListAdapter<MediaFile, GalleryAdapter.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class DiffCallback : DiffUtil.ItemCallback<MediaFile>() {

        override fun areItemsTheSame(oldItem: MediaFile, newItem: MediaFile): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MediaFile, newItem: MediaFile): Boolean {
            return oldItem == newItem
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val ivThumbnail: AppCompatImageView = itemView.findViewById(R.id.ivThumbnail)
        private val tvFileName: AppCompatTextView = itemView.findViewById(R.id.tvFileName)
        private val chbSelected: AppCompatCheckBox = itemView.findViewById(R.id.chbSelected)

        fun bind(mediaFile: MediaFile) {
            if (mediaFile.mimeType == TYPE_IMAGE || mediaFile.absolutePath != null) {
                Picasso.get()
                    .load(File(mediaFile.absolutePath!!))
                    .fit()
                    .centerInside()
                    .into(ivThumbnail)
            } else {
                ivThumbnail.setImageDrawable(ColorDrawable(Color.LTGRAY))
            }
            tvFileName.text = mediaFile.name
            chbSelected.visibility = View.GONE
        }

        companion object {

            private const val TYPE_IMAGE = MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
            private const val TYPE_VIDEO = MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO


            @JvmStatic
            fun create(parent: ViewGroup): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)

                return ViewHolder(inflater.inflate(R.layout.list_item_media_file, parent, false))
            }
        }
    }
}