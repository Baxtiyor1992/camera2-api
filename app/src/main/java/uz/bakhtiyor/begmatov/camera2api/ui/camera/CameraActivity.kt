package uz.bakhtiyor.begmatov.camera2api.ui.camera

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.hardware.camera2.CameraManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.Gravity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_camera.*
import uz.bakhtiyor.begmatov.camera2api.App
import uz.bakhtiyor.begmatov.camera2api.AppSettings
import uz.bakhtiyor.begmatov.camera2api.R
import uz.bakhtiyor.begmatov.camera2api.data.repos.MediaFilesRepo
import uz.bakhtiyor.begmatov.camera2api.ui.core.BaseActivity
import uz.bakhtiyor.begmatov.camera2api.utils.CameraController
import uz.bakhtiyor.begmatov.camera2api.utils.PhotoCameraController
import uz.bakhtiyor.begmatov.camera2api.utils.VideoCameraController


class CameraActivity : BaseActivity() {

    private lateinit var appSettings: AppSettings
    private lateinit var mediaFilesRepo: MediaFilesRepo
    private lateinit var cameraManager: CameraManager

    private var cameraController: CameraController? = null
    private var isRecordingVideo = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        resolveDependencies()
        setupUi()
    }

    override fun onResume() {
        super.onResume()

        startCameraPreview()
    }

    override fun onStop() {
        super.onStop()

        cameraController?.stopPreview()
    }

    private fun resolveDependencies() {
        appSettings = App.get(this).settings
        mediaFilesRepo = App.get(this).mediaFilesRepo
        cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }

    private fun setupUi() {
        cameraPreview.addOnLayoutChangeListener { _, left, top, right, bottom, _, _, _, _ ->
            rectangleView.layoutParams.width = right - left
            rectangleView.layoutParams.height = bottom - top
            rectangleView.requestLayout()
        }

        btnChangeCamera.setOnClickListener { changeCamera() }
        btnChangeCameraMode.setOnClickListener { changeCameraMode() }
        updateCameraModeIcon()

        btnCapture.setOnClickListener {
            if (cameraController is PhotoCameraController) {
                (cameraController as PhotoCameraController).takePhoto()
            } else if (cameraController is VideoCameraController) {
                val videoCameraController = cameraController as VideoCameraController

                isRecordingVideo = if (!isRecordingVideo) {
                    videoCameraController.startRecording()
                    btnChangeCameraMode.visibility = View.GONE
                    true
                } else {
                    videoCameraController.stopRecording()
                    btnChangeCameraMode.visibility = View.VISIBLE
                    false
                }
            }
        }
        recreateCameraController()
    }

    private fun changeCamera() {
        val selectedCameraId = getSelectedCameraId()

        for (cameraId in cameraManager.cameraIdList) {
            if (cameraId != selectedCameraId) {
                appSettings.setSelectedCameraId(cameraId)

                recreateCameraController()
                startCameraPreview()
            }
        }
    }

    private fun changeCameraMode() {
        val selectedCameraMode =
            if (appSettings.getSelectedCameraMode() == AppSettings.CAMERA_MODE_PHOTO) {
                AppSettings.CAMERA_MODE_VIDEO
            } else {
                AppSettings.CAMERA_MODE_PHOTO
            }

        appSettings.setSelectedCameraMode(selectedCameraMode)
        updateCameraModeIcon()
        recreateCameraController()
        startCameraPreview()
    }

    private fun updateCameraModeIcon() {
        val modeName: String
        btnChangeCameraMode.setImageResource(
            if (getSelectedCameraMode() == AppSettings.CAMERA_MODE_PHOTO) {
                modeName = getString(R.string.photo_mode)
                R.drawable.ic_videocam_white_24dp

            } else {
                modeName = getString(R.string.video_mode)
                R.drawable.ic_photo_camera_white_24dp
            }
        )
        val toast = Toast.makeText(this, modeName, Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }

    private fun recreateCameraController() {
        cameraController?.stopPreview()
        cameraController = createCameraController()
    }

    private fun createCameraController(): CameraController {
        return if (getSelectedCameraMode() == AppSettings.CAMERA_MODE_PHOTO) {
            PhotoCameraController(this, getSelectedCameraId(), mediaFilesRepo)
        } else {
            VideoCameraController(this, getSelectedCameraId(), mediaFilesRepo)
        }
    }

    private fun getSelectedCameraId(): String {
        return appSettings.getSelectedCameraId() ?: cameraManager.cameraIdList[0]
    }

    private fun getSelectedCameraMode(): Int {
        return appSettings.getSelectedCameraMode()
    }

    @SuppressWarnings("MissingPermission")
    private fun startCameraPreview() {
        if (hasPermission(Manifest.permission.CAMERA)) {
            cameraController?.startPreview(cameraPreview)
        }
    }

    companion object {

        @JvmStatic
        fun start(context: Context) {
            context.startActivity(Intent(context, CameraActivity::class.java))
        }
    }
}