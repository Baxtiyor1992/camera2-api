package uz.bakhtiyor.begmatov.camera2api.ui.gallery

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import uz.bakhtiyor.begmatov.camera2api.App
import uz.bakhtiyor.begmatov.camera2api.R
import uz.bakhtiyor.begmatov.camera2api.data.models.MediaFile
import uz.bakhtiyor.begmatov.camera2api.data.repos.MediaFilesRepo
import uz.bakhtiyor.begmatov.camera2api.data.tasks.GetMediaFilesAsyncTask
import uz.bakhtiyor.begmatov.camera2api.utils.DimenConverter

class GalleryFragment : Fragment() {

    private lateinit var mediaFilesRepo: MediaFilesRepo
    private val galleryAdapter = GalleryAdapter()

    private var swipeRefresh: SwipeRefreshLayout? = null
    private var rvMediaFiles: RecyclerView? = null

    private val mediaFiles = mutableListOf<MediaFile>()

    override fun onAttach(context: Context) {
        super.onAttach(context)

        mediaFilesRepo = App.get(context).mediaFilesRepo
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val dimenConverter = DimenConverter(view.context)
        val spanCount = resources.displayMetrics.widthPixels / dimenConverter.dp2px(120F)


        rvMediaFiles = view.findViewById(R.id.rvMediaFiles)
        rvMediaFiles!!.layoutManager = GridLayoutManager(view.context, spanCount.toInt())
        rvMediaFiles!!.adapter = galleryAdapter

        swipeRefresh = view.findViewById(R.id.swipeRefresh)
        swipeRefresh!!.setOnRefreshListener { loadMediaFilesList() }

        if (mediaFiles.isEmpty()) {
            loadMediaFilesList()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        rvMediaFiles = null
        swipeRefresh = null
    }

    private fun loadMediaFilesList() {
        swipeRefresh?.isRefreshing = true
        GetMediaFilesAsyncTask(mediaFilesRepo) {
            swipeRefresh?.isRefreshing = false
            mediaFiles.clear()
            mediaFiles.addAll(it)
            galleryAdapter.submitList(ArrayList(it))
        }.execute()
    }

    companion object {

        @JvmStatic
        fun newInstance(): GalleryFragment =
            GalleryFragment()
    }
}