package uz.bakhtiyor.begmatov.camera2api.ui.gallery

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_gallery.*
import uz.bakhtiyor.begmatov.camera2api.R
import uz.bakhtiyor.begmatov.camera2api.ui.camera.CameraActivity
import uz.bakhtiyor.begmatov.camera2api.ui.core.BaseActivity

class GalleryActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)

        fabOpenCamera.setOnClickListener { CameraActivity.start(this) }

        if (hasPermissions(REQUIRED_PERMISSIONS)) {
            startGalleryFragment()
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, PERMISSION_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (hasPermissions(REQUIRED_PERMISSIONS)) {
                startGalleryFragment()
            } else {
                showPermissionRationale()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showPermissionRationale() {
        AlertDialog.Builder(this)
            .setMessage(R.string.permissions_rationale_message)
            .setPositiveButton(R.string.allow) { _, _ ->
                if (shouldShowRequestPermissionRationale(REQUIRED_PERMISSIONS)) {
                    ActivityCompat.requestPermissions(
                        this,
                        REQUIRED_PERMISSIONS,
                        PERMISSION_REQUEST_CODE
                    )
                } else {
                    openAppSettings()
                }
            }
            .setNegativeButton(R.string.exit) { _, _ -> finish() }
            .show()
    }

    private fun startGalleryFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.containerLayout, GalleryFragment.newInstance())
            .commitAllowingStateLoss()
    }

    private fun openAppSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivity(intent)
    }

    companion object {

        private const val PERMISSION_REQUEST_CODE = 1

        @JvmStatic
        private val REQUIRED_PERMISSIONS = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
    }
}
