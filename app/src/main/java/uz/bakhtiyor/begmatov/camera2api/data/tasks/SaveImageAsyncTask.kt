package uz.bakhtiyor.begmatov.camera2api.data.tasks

import android.os.AsyncTask
import uz.bakhtiyor.begmatov.camera2api.data.models.PhotoImage
import uz.bakhtiyor.begmatov.camera2api.data.repos.MediaFilesRepo

class SaveImageAsyncTask(private val mediaFilesRepo: MediaFilesRepo) :
    AsyncTask<PhotoImage, Void, Void?>() {

    override fun doInBackground(vararg params: PhotoImage?): Void? {
        params.filterNotNull().forEach {
            mediaFilesRepo.saveMediaFile(it.image, it.fileName)
            it.image.close()
        }
        return null
    }
}