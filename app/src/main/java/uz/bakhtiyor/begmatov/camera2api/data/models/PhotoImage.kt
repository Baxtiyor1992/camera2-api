package uz.bakhtiyor.begmatov.camera2api.data.models

import android.media.Image

data class PhotoImage(val image: Image, val fileName: String)