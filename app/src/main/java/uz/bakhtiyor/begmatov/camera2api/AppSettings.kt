package uz.bakhtiyor.begmatov.camera2api

import android.content.Context
import android.content.SharedPreferences

class AppSettings private constructor(context: Context) {

    private val prefs: SharedPreferences

    init {
        prefs = context.applicationContext.getSharedPreferences(
            SETTINGS_FILE_NAME,
            Context.MODE_PRIVATE
        )
    }

    fun getSelectedCameraId(): String? {
        return prefs.getString(KEY_SELECTED_CAMERA_ID, null)
    }

    fun setSelectedCameraId(cameraId: String?) {
        val editor = prefs.edit()

        if (cameraId != null) {
            editor.putString(KEY_SELECTED_CAMERA_ID, cameraId)
        } else {
            editor.remove(KEY_SELECTED_CAMERA_ID)
        }

        editor.apply()
    }

    fun getSelectedCameraMode(): Int {
        return prefs.getInt(KEY_SELECTED_CAMERA_MODE, CAMERA_MODE_PHOTO)
    }

    fun setSelectedCameraMode(selectedCameraMode: Int?) {
        val editor = prefs.edit()

        if (selectedCameraMode != null) {
            editor.putInt(KEY_SELECTED_CAMERA_MODE, selectedCameraMode)
        } else {
            editor.remove(KEY_SELECTED_CAMERA_MODE)
        }

        editor.apply()
    }

    companion object {

        private const val SETTINGS_FILE_NAME = "app.settings"
        private const val KEY_SELECTED_CAMERA_ID = "selected_camera_id"
        private const val KEY_SELECTED_CAMERA_MODE = "selected_camera_mode"

        const val CAMERA_MODE_PHOTO = 1
        const val CAMERA_MODE_VIDEO = 2

        @JvmStatic
        private var instance: AppSettings? = null

        @JvmStatic
        fun getInstance(context: Context): AppSettings {
            if (instance == null) {
                instance = AppSettings(context)
            }
            return instance!!
        }
    }
}