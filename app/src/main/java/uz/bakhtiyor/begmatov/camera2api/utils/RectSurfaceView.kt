package uz.bakhtiyor.begmatov.camera2api.utils

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceView
import uz.bakhtiyor.begmatov.camera2api.R
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

class RectSurfaceView : SurfaceView {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val rect = Rect()
    private val dimenConverter = DimenConverter(context)

    private var touchX: Int = 0
    private var touchY: Int = 0
    private var rectWidth = dimenConverter.dp2px(80F).toInt()
    private var rectHeight = dimenConverter.dp2px(80F).toInt()

    private var showRect = 0L

    private val invalidateRunnable = Runnable {
        holder.lockCanvas().let {
            clearCanvas(it)

            if (System.currentTimeMillis() <= showRect) {
                it.drawRect(rect, paint)
            }

            holder.unlockCanvasAndPost(it)
        }
    }

    constructor(context: Context) : super(context) {
        setWillNotDraw(false)
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        setWillNotDraw(false)
        init(context)
    }

    private fun init(context: Context, attrs: AttributeSet? = null) {
        holder.setFormat(PixelFormat.TRANSPARENT)
        setZOrderOnTop(true)

        paint.style = Paint.Style.STROKE
        paint.color = Color.WHITE
        paint.strokeWidth = dimenConverter.dp2px(2F)

        val typedArray = context.theme
            .obtainStyledAttributes(
                attrs,
                R.styleable.RectSurfaceView, 0, 0
            )

        try {
            rectWidth = typedArray.getDimensionPixelSize(
                R.styleable.RectSurfaceView_rectWidth,
                rectWidth
            )
            rectHeight = typedArray.getDimensionPixelSize(
                R.styleable.RectSurfaceView_rectHeight,
                rectHeight
            )
        } finally {
            typedArray.recycle()
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        touchX = max((right + left - rectWidth) / 2, rectWidth)
        touchY = max((bottom + top - rectHeight) / 2, rectHeight)

        rect.set(touchX, touchY, touchX + rectWidth, touchY + rectHeight)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return when (event?.action) {
            MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                touchX = max(event.x.roundToInt() - rectWidth / 2, 0)
                touchY = max(event.y.roundToInt() - rectHeight / 2, 0)

                touchX = min(touchX, right - rectWidth)
                touchY = min(touchY, bottom - rectHeight)

                rect.set(touchX, touchY, touchX + rectWidth, touchY + rectHeight)
                showRect = System.currentTimeMillis() + RECT_VISIBLE_TIMEOUT

                if (holder.surface.isValid) {
                    invalidateRunnable.run()
                    handler.removeCallbacks(invalidateRunnable)
                    handler.postDelayed(invalidateRunnable, RECT_VISIBLE_TIMEOUT)
                }
                true
            }
            else -> super.onTouchEvent(event)
        }
    }

    private fun clearCanvas(canvas: Canvas) {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        canvas.drawColor(Color.TRANSPARENT)
    }

    companion object {

        private const val RECT_VISIBLE_TIMEOUT = 1500L
    }
}