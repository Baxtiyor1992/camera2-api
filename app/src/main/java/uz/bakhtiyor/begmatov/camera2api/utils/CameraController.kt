package uz.bakhtiyor.begmatov.camera2api.utils

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.graphics.Matrix
import android.graphics.RectF
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.hardware.camera2.params.StreamConfigurationMap
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.util.Size
import android.util.SparseIntArray
import android.view.Surface
import android.view.TextureView
import androidx.annotation.RequiresPermission
import kotlin.math.max

abstract class CameraController(
    private val activity: Activity,
    private val cameraId: String
) : TextureView.SurfaceTextureListener {

    private var cameraManager: CameraManager =
        activity.getSystemService(Context.CAMERA_SERVICE) as CameraManager

    private var cameraDevice: CameraDevice? = null

    private var characteristics: CameraCharacteristics
    private var cameraStreamConfig: StreamConfigurationMap
    private var cameraSize: Size
    private var cameraOrientation = 0

    private var cameraRenderThread: HandlerThread? = null
    private var renderThreadHandler: Handler? = null
    private var mainThreadHandler = Handler(Looper.getMainLooper())

    private val isFrontCamera: Boolean
    private val isFlashAvailable: Boolean

    private var display: AutoFitTextureView? = null

    private var previewRequestBuilder: CaptureRequest.Builder? = null
    private var normalPreviewRequest: CaptureRequest? = null
    private var captureSession: CameraCaptureSession? = null
    private val cameraCallback: CameraDevice.StateCallback

    init {
        characteristics = cameraManager.getCameraCharacteristics(cameraId)
        cameraStreamConfig =
            characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!

        isFrontCamera = characteristics.get(CameraCharacteristics.LENS_FACING) ==
                CameraCharacteristics.LENS_FACING_FRONT
        isFlashAvailable = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE) ?: false

        cameraSize = getMaxSize(cameraStreamConfig.getOutputSizes(SurfaceTexture::class.java))
            ?: throw IllegalArgumentException("Unable get camera output size")
        cameraOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION) ?: 0

        cameraCallback = object : CameraDevice.StateCallback() {

            override fun onOpened(camera: CameraDevice) {
                cameraDevice = camera
                resizeDisplay(cameraSize.width, cameraSize.height)

                createPreviewSession()
            }

            override fun onDisconnected(camera: CameraDevice) {

            }

            override fun onError(camera: CameraDevice, error: Int) {

            }
        }
    }

    @RequiresPermission(android.Manifest.permission.CAMERA)
    fun startPreview(display: AutoFitTextureView) {
        startRenderThread()

        this.display = display

        if (display.isAvailable) {
            resizeDisplayAndStartPreview(display.width, display.height)
        } else {
            display.surfaceTextureListener = this
        }
    }

    open fun stopPreview() {
        closeCaptureSession()
        closeCamera()
        stopRenderThread()
    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?, width: Int, height: Int) {
        resizeDisplay(width, height)
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) {

    }

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean {
        return true
    }


    private fun startRenderThread() {
        cameraRenderThread = HandlerThread("CameraRenderThread").also { it.start() }
        renderThreadHandler = Handler(cameraRenderThread!!.looper)
    }

    private fun stopRenderThread() {
        cameraRenderThread?.quitSafely()

        var tryAgain = true
        while (tryAgain) {
            tryAgain = try {
                cameraRenderThread?.join()
                false
            } catch (e: InterruptedException) {
                e.printStackTrace()
                true
            }
        }
    }

    @RequiresPermission(android.Manifest.permission.CAMERA)
    override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
        resizeDisplayAndStartPreview(width, height)
    }

    fun getActivity(): Activity {
        return activity
    }

    fun getCameraDevice(): CameraDevice? {
        return cameraDevice
    }

    fun getCameraStreamConfig(): StreamConfigurationMap {
        return cameraStreamConfig
    }

    fun getCameraOrientation(): Int {
        return cameraOrientation
    }

    fun getDisplay(): AutoFitTextureView? {
        return display
    }

    fun getRenderThreadHandler(): Handler? {
        return renderThreadHandler
    }

    fun getPreviewRequestBuilder(): CaptureRequest.Builder? {
        return previewRequestBuilder
    }

    fun getNormalPreviewRequest(): CaptureRequest? {
        return normalPreviewRequest
    }

    fun getCaptureSession(): CameraCaptureSession? {
        return captureSession
    }

    protected fun setContinuousAutoFocus(captureRequestBuilder: CaptureRequest.Builder) {
        captureRequestBuilder.set(
            CaptureRequest.CONTROL_AF_MODE,
            CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE
        )
    }

    protected fun resetAutoFocus(captureRequestBuilder: CaptureRequest.Builder) {
        captureRequestBuilder.set(
            CaptureRequest.CONTROL_AF_TRIGGER,
            CameraMetadata.CONTROL_AF_TRIGGER_CANCEL
        )
    }

    protected fun setAutoFlash(captureRequestBuilder: CaptureRequest.Builder) {
        if (isFlashAvailable) {
            captureRequestBuilder.set(
                CaptureRequest.CONTROL_AE_MODE,
                CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH
            )
        }
    }

    protected fun setOrientation(captureRequestBuilder: CaptureRequest.Builder) {
        captureRequestBuilder.set(
            CaptureRequest.JPEG_ORIENTATION,
            getOrientation(activity.windowManager.defaultDisplay.rotation)
        )
    }

    @RequiresPermission(android.Manifest.permission.CAMERA)
    private fun resizeDisplayAndStartPreview(width: Int, height: Int) {
        display?.apply {
            when (context.resources.configuration.orientation) {
                Configuration.ORIENTATION_LANDSCAPE -> setRatio(cameraSize.width, cameraSize.height)
                else -> setRatio(cameraSize.height, cameraSize.width)
            }
        }
        resizeDisplay(width, height)
        cameraManager.openCamera(cameraId, cameraCallback, renderThreadHandler)
    }

    private fun createPreviewSession() {
        closeCaptureSession()
        display?.surfaceTexture?.setDefaultBufferSize(cameraSize.width, cameraSize.height)
        val previewSurface = Surface(display!!.surfaceTexture)

        val previewRequestBuilder =
            cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)

        previewRequestBuilder.addTarget(previewSurface)
        setupPreviewRequestBuilder(previewRequestBuilder)

        this.previewRequestBuilder = previewRequestBuilder
        val outputSurfaces = mutableListOf(previewSurface).apply {
            addAll(getExtraOutputSurfaces())
        }

        cameraDevice!!.createCaptureSession(
            outputSurfaces,
            object : CameraCaptureSession.StateCallback() {

                override fun onConfigureFailed(session: CameraCaptureSession) {

                }

                override fun onConfigured(session: CameraCaptureSession) {
                    captureSession = session
                    normalPreviewRequest = previewRequestBuilder.build()
                    session.setRepeatingRequest(
                        previewRequestBuilder.build(),
                        getCaptureCallback(),
                        renderThreadHandler
                    )
                }
            },
            renderThreadHandler
        )
    }

    protected fun closeCaptureSession() {
        captureSession?.close()
        captureSession = null
    }

    private fun closeCamera() {
        cameraDevice?.close()
        cameraDevice = null
    }

    private fun resizeDisplay(width: Int, height: Int) {
        val matrix = Matrix()
        val rotation = activity.windowManager.defaultDisplay.rotation

        val viewRect = RectF(0F, 0F, width.toFloat(), height.toFloat())
        val previewRect = RectF(0F, 0F, display!!.height.toFloat(), display!!.width.toFloat())

        if (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) {
            val centerX = viewRect.centerX()
            val centerY = viewRect.centerY()

            previewRect.offset(centerX - previewRect.centerX(), centerY - previewRect.centerY())
            matrix.setRectToRect(viewRect, previewRect, Matrix.ScaleToFit.FILL)
            val scale = max(
                viewRect.width() / previewRect.width(),
                viewRect.height() / previewRect.height()
            )

            matrix.postScale(scale, scale, centerX, centerY)
            matrix.postRotate(90F * (rotation - 2), centerX, centerY)
        }

        mainThreadHandler.post { display?.setTransform(matrix) }
    }

    private fun getOrientation(rotation: Int): Int {
        return (DEFAULT_ORIENTATIONS.get(rotation) + cameraOrientation + 270) % 360
    }

    abstract fun setupPreviewRequestBuilder(previewRequestBuilder: CaptureRequest.Builder)

    abstract fun getCaptureCallback(): CameraCaptureSession.CaptureCallback?

    abstract fun getExtraOutputSurfaces(): List<Surface>

    companion object {

        @JvmStatic
        protected val DEFAULT_ORIENTATIONS = SparseIntArray().apply {
            append(Surface.ROTATION_0, 90)
            append(Surface.ROTATION_90, 0)
            append(Surface.ROTATION_180, 270)
            append(Surface.ROTATION_270, 180)
        }

        @JvmStatic
        protected val INVERSE_ORIENTATIONS = SparseIntArray().apply {
            append(Surface.ROTATION_0, 270)
            append(Surface.ROTATION_90, 180)
            append(Surface.ROTATION_180, 90)
            append(Surface.ROTATION_270, 0)
        }

        @JvmStatic
        fun getMaxSize(cameraSizes: Array<out Size>): Size? {
            var max: Size? = null

            cameraSizes.forEach {
                if (max == null || max!!.width * max!!.height < it.width * it.height) {
                    max = it
                }
            }

            return max
        }
    }
}