package uz.bakhtiyor.begmatov.camera2api.utils

import android.app.Activity
import android.graphics.ImageFormat
import android.hardware.camera2.*
import android.media.ImageReader
import android.media.MediaActionSound
import android.view.Surface
import uz.bakhtiyor.begmatov.camera2api.data.repos.MediaFilesRepo
import uz.bakhtiyor.begmatov.camera2api.data.models.PhotoImage
import uz.bakhtiyor.begmatov.camera2api.data.tasks.SaveImageAsyncTask
import java.text.SimpleDateFormat
import java.util.*

class PhotoCameraController(
    activity: Activity,
    cameraId: String,
    mediaFilesRepo: MediaFilesRepo
) : CameraController(activity, cameraId) {

    private var state = STATE_PREVIEW

    private var imageReader: ImageReader?
    private val onImageAvailableListener = ImageReader.OnImageAvailableListener {
        SaveImageAsyncTask(
            mediaFilesRepo
        ).execute(
            PhotoImage(
                it.acquireNextImage(),
                getNameForNewImage()
            )
        )
    }

    private val captureCallback = object : CameraCaptureSession.CaptureCallback() {

        override fun onCaptureCompleted(
            session: CameraCaptureSession,
            request: CaptureRequest,
            result: TotalCaptureResult
        ) {
            progress(result)
        }

        override fun onCaptureProgressed(
            session: CameraCaptureSession,
            request: CaptureRequest,
            partialResult: CaptureResult
        ) {
            progress(partialResult)
        }

        private fun progress(captureResult: CaptureResult) {
            when (state) {
                STATE_PREVIEW -> {
                }
                STATE_WAITING_LOCK -> {
                    when (getAutoFocusState(captureResult)) {
                        CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED,
                        CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED,
                        CaptureResult.CONTROL_AF_STATE_INACTIVE -> {
                            when (getAutoExposureState(captureResult)) {
                                CaptureResult.CONTROL_AE_STATE_CONVERGED,
                                null -> {
                                    state = STATE_PICTURE_TAKEN
                                    captureStillPicture()
                                }
                                else -> runPreCaptureSequence()
                            }
                        }
                        null -> captureStillPicture()
                    }
                }
                STATE_WAITING_PRE_CAPTURE -> {
                    when (getAutoExposureState(captureResult)) {
                        CaptureResult.CONTROL_AE_STATE_PRECAPTURE,
                        CaptureResult.CONTROL_AE_STATE_FLASH_REQUIRED,
                        null -> state = STATE_WAITING_NON_PRE_CAPTURE
                    }
                }
                STATE_WAITING_NON_PRE_CAPTURE -> {
                    when (getAutoExposureState(captureResult)) {
                        CaptureResult.CONTROL_AE_STATE_PRECAPTURE,
                        null -> {
                            state = STATE_PICTURE_TAKEN
                            captureStillPicture()
                        }
                    }
                }
            }
        }
    }

    init {
        val imageSize = getMaxSize(getCameraStreamConfig().getOutputSizes(ImageReader::class.java))
            ?: throw RuntimeException("Unable get output image size")

        imageReader = ImageReader
            .newInstance(imageSize.width, imageSize.height, ImageFormat.JPEG, 2)
            .also {
                it.setOnImageAvailableListener(onImageAvailableListener, getRenderThreadHandler())
            }
    }

    override fun stopPreview() {
        super.stopPreview()
        closeImageReader()
    }

    override fun setupPreviewRequestBuilder(previewRequestBuilder: CaptureRequest.Builder) {
        setContinuousAutoFocus(previewRequestBuilder)
        setAutoFlash(previewRequestBuilder)
    }

    override fun getCaptureCallback(): CameraCaptureSession.CaptureCallback? {
        return captureCallback
    }

    override fun getExtraOutputSurfaces(): List<Surface> {
        val extraOutputSurfaces = mutableListOf<Surface>()

        imageReader?.let {
            extraOutputSurfaces.add(it.surface)
        }
        return extraOutputSurfaces
    }

    fun takePhoto() {
        lockFocus()
    }

    private fun lockFocus() {
        val previewRequestBuilder = getPreviewRequestBuilder() ?: return

        previewRequestBuilder.set(
            CaptureRequest.CONTROL_AF_TRIGGER,
            CameraMetadata.CONTROL_AF_TRIGGER_START
        )
        state = STATE_WAITING_LOCK

        getCaptureSession()!!.capture(
            previewRequestBuilder.build(),
            getCaptureCallback(),
            getRenderThreadHandler()
        )
    }

    private fun runPreCaptureSequence() {
        val previewRequestBuilder = getPreviewRequestBuilder() ?: return

        previewRequestBuilder.set(
            CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
            CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START
        )
        state = STATE_WAITING_PRE_CAPTURE

        getCaptureSession()!!.capture(
            previewRequestBuilder.build(),
            getCaptureCallback(),
            getRenderThreadHandler()
        )
    }

    private fun captureStillPicture() {
        val captureRequestBuilder = getCameraDevice()!!.createCaptureRequest(
            CameraDevice.TEMPLATE_STILL_CAPTURE
        )

        captureRequestBuilder.addTarget(imageReader!!.surface)
        setContinuousAutoFocus(captureRequestBuilder)
        setAutoFlash(captureRequestBuilder)
        setOrientation(captureRequestBuilder)

        getCaptureSession()?.let {
            it.stopRepeating()
            it.abortCaptures()
            it.capture(
                captureRequestBuilder.build(),
                object : CameraCaptureSession.CaptureCallback() {
                    override fun onCaptureStarted(
                        session: CameraCaptureSession,
                        request: CaptureRequest,
                        timestamp: Long,
                        frameNumber: Long
                    ) {
                        MediaActionSound().play(MediaActionSound.SHUTTER_CLICK)
                    }

                    override fun onCaptureCompleted(
                        session: CameraCaptureSession,
                        request: CaptureRequest,
                        result: TotalCaptureResult
                    ) {
                        unlockFocus()
                    }
                },
                getRenderThreadHandler()
            )
        }
    }

    private fun unlockFocus() {
        val previewRequestBuilder = getPreviewRequestBuilder() ?: return

        resetAutoFocus(previewRequestBuilder)
        setAutoFlash(previewRequestBuilder)

        getCaptureSession()!!.capture(
            previewRequestBuilder.build(),
            getCaptureCallback(),
            getRenderThreadHandler()
        )

        state = STATE_PREVIEW
        getCaptureSession()?.setRepeatingRequest(
            getNormalPreviewRequest()!!,
            getCaptureCallback(),
            getRenderThreadHandler()
        )
    }

    private fun closeImageReader() {
        imageReader?.close()
        imageReader = null
    }

    private fun getAutoFocusState(captureResult: CaptureResult): Int? {
        return captureResult.get(CaptureResult.CONTROL_AF_STATE)
    }

    private fun getAutoExposureState(captureResult: CaptureResult): Int? {
        return captureResult.get(CaptureResult.CONTROL_AE_STATE)
    }

    companion object {

        private const val STATE_PREVIEW = 0
        private const val STATE_WAITING_LOCK = 1
        private const val STATE_WAITING_PRE_CAPTURE = 2
        private const val STATE_WAITING_NON_PRE_CAPTURE = 3
        private const val STATE_PICTURE_TAKEN = 4

        @JvmStatic
        private val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)

        @JvmStatic
        private fun getNameForNewImage(): String {
            return "Photo ${dateFormat.format(Date(System.currentTimeMillis()))}.jpg"
        }
    }
}