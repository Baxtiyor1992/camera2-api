package uz.bakhtiyor.begmatov.camera2api.utils

import android.app.Activity
import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraMetadata
import android.hardware.camera2.CaptureRequest
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.util.Size
import android.view.Surface
import uz.bakhtiyor.begmatov.camera2api.data.repos.MediaFilesRepo
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class VideoCameraController(
    activity: Activity,
    cameraId: String,
    mediaFilesRepo: MediaFilesRepo
) : CameraController(activity, cameraId) {

    private var videoSize: Size = getMaxSize(
        getCameraStreamConfig().getOutputSizes(MediaRecorder::class.java)
    ) ?: throw RuntimeException("Unable get output video size")

    private var mediaRecorder: MediaRecorder? = null
    private var outputFile: File? = null

    private var recordSession: CameraCaptureSession? = null

    override fun stopPreview() {
        super.stopPreview()
        closeMediaRecorder()
    }

    fun startRecording() {
        val display = getDisplay()

        if (display == null || !display.isAvailable) {
            return
        }
        closeCaptureSession()
        setUpMediaRecorder()


        val previewSurface = Surface(display.surfaceTexture)
        val recorderSurface = mediaRecorder!!.surface

        val surfaces = listOf(
            previewSurface,
            recorderSurface
        )
        val recordRequestBuilder = getCameraDevice()!!
            .createCaptureRequest(CameraDevice.TEMPLATE_RECORD)
            .apply {
                addTarget(previewSurface)
                addTarget(recorderSurface)
            }

        setupPreviewRequestBuilder(recordRequestBuilder)
        getCameraDevice()!!.createCaptureSession(
            surfaces,
            object : CameraCaptureSession.StateCallback() {

                override fun onConfigureFailed(session: CameraCaptureSession) {

                }

                override fun onConfigured(session: CameraCaptureSession) {
                    recordSession = session
                    session.setRepeatingRequest(
                        recordRequestBuilder.build(),
                        null,
                        null
                    )
                    mediaRecorder?.start()
                }
            },
            getRenderThreadHandler()
        )
    }

    fun stopRecording() {
        recordSession!!.stopRepeating()
        recordSession!!.abortCaptures()

        mediaRecorder?.apply {
            stop()
            reset()
        }

        closeCaptureSession()
    }

    override fun setupPreviewRequestBuilder(previewRequestBuilder: CaptureRequest.Builder) {
        previewRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO)
    }

    override fun getCaptureCallback(): CameraCaptureSession.CaptureCallback? {
        return null
    }

    override fun getExtraOutputSurfaces(): List<Surface> {
        return listOf()
    }

    private fun setUpMediaRecorder() {
        val rotation = getActivity().windowManager.defaultDisplay.rotation
        outputFile = File(getActivity().filesDir, getNameForNewVideo())


        val profile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P)

        mediaRecorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setVideoSource(MediaRecorder.VideoSource.SURFACE)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(outputFile!!.absolutePath)
            setVideoEncodingBitRate(profile.videoBitRate)
            setVideoFrameRate(profile.videoFrameRate)
            setVideoSize(videoSize.width, videoSize.height)
            setVideoEncoder(MediaRecorder.VideoEncoder.H264)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

            setAudioEncodingBitRate(profile.audioBitRate)
            setAudioSamplingRate(profile.audioSampleRate)
        }

        when (getCameraOrientation()) {
            SENSOR_ORIENTATION_DEFAULT_DEGREES -> {
                mediaRecorder!!.setOrientationHint(DEFAULT_ORIENTATIONS[rotation])
            }
            SENSOR_ORIENTATION_INVERSE_DEGREES -> {
                mediaRecorder!!.setOrientationHint(INVERSE_ORIENTATIONS[rotation])
            }
        }

        mediaRecorder!!.prepare()
    }

    private fun closeMediaRecorder() {
        mediaRecorder?.release()
        mediaRecorder = null
    }

    companion object {

        private const val SENSOR_ORIENTATION_DEFAULT_DEGREES = 90
        private const val SENSOR_ORIENTATION_INVERSE_DEGREES = 270

        @JvmStatic
        private val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)

        @JvmStatic
        private fun getNameForNewVideo(): String {
            return "Video ${dateFormat.format(Date(System.currentTimeMillis()))}.mp4"
        }
    }
}